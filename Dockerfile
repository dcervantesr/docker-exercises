FROM nginx:1.19.3-alpine
EXPOSE 8080
HEALTHCHECK --interval=45s --timeout=5s --start-period=15s --retries=2 \
    CMD curl -f http://localhost/ || exit 1
COPY /hw-01/new-index /usr/share/nginx/html
WORKDIR /static_content
COPY /hw-01/new-index .
VOLUME /static_content