# Tuve qye subir a la version 7.13.4 porque la 7.9.3 me daba este error en la mac: no matching manifest for linux/arm64/v8 in the manifest list entries

version: '3.6'
services:
  elasticsearch:
    container_name: elasticsearch
    image: elasticsearch:7.13.4
    environment:
      - discovery.type=single-node 
    networks:
      - elastic
    ports:
      - 9200:9200
      - 9300:9300
  kibana:
    container_name: kibana
    image: kibana:7.13.4
    networks:
      - elastic
    environment:
      - ELASTICSEARCH_HOST=elasticsearch
      - ELASTICSEARCH_PORT=9200
    ports:
      - 5601:5601
    depends_on:
      - elasticsearch
networks:
  elastic:
    driver: bridge