# Pasos realizados:
1. Se declara la imagen base que será utilizada mediante FROM.
2. Con EXPOSE indicamos el puerto por el cual nos conectaremos.
3. En la tercera línea hacemos una copia del contenido de 'new-index' hacia la carpeta '/usr/share/nginx/html' de dentro de la imagen.
4. Luego creamos la carpeta 'static_content' y nos movemos adentro de ella, ambas cosas con WORKDIR.
5. Copiamos el contenido de 'new-index' hacia dentro de la carpeta 'static_content'.
6. Creamos el volumen

# Ficheros utilizados
* Dockerfile
FROM nginx:1.19.3-alpine
EXPOSE 8080
COPY /hw-01/new-index /usr/share/nginx/html
WORKDIR /static_content
COPY /hw-01/new-index .
VOLUME /static_content

* Carpeta new-index con el fichero index.html
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Ejercicio 3 de Docker</title>
    </head>
    <body>
        <h1>HOMEWORK 1</h1>
    </body>
</html>
