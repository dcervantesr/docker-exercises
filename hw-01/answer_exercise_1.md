## Indica la diferencia entre el uso de la instrucción CMD y ENTRYPOINT (Dockerfile)

El comando que se define en el ENTRYPOINT se ejecuta al levantar el contendor y no puede ser pisado.
En cambio CMD son lo argumentos que se agregan al ENTRYPOINT y que si pueden ser modificados.