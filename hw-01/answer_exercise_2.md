## Indica la diferencia entre el uso de la instrucción ADD y COPY (Dockerfile)

Si bien los dos comandos permiten copiar ficheros, COPY solo permite copiar desde una carpeta local o un host local. ADD permite lo anterior pero además puede copiar desde una url o un .tar, en este último caso también extrae el contenido del comprimido.